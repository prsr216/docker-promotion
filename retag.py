import os
def ImagePromotion():
    Registry = os.environ['dockerRegistry']
    FromRepo = os.environ['sourceRepo']
    ToRepo = os.environ['targetRepo']
    images = os.environ['imageMetadata'].split(",")
    print ("Docker Registry: {}".format(Registry))
    print ("Source Repository: {}".format(FromRepo))
    print ("Target Repository: {}".format(ToRepo))
    print ("List of images to promote: {}".format(images))
    for imageName in images:
        current_tag = Registry + "/" + FromRepo + "/" + imageName
        new_tag = Registry + "/" + ToRepo + "/" + imageName
        print ("\n")
        print ("Promoting image: {}".format(imageName))
        print("current tag: {}".format(current_tag))
        print ("new tag: {}".format(new_tag))
        cmd = f'docker pull {current_tag}'
        print ("Command to pull image: {}".format(cmd))
        os.system(cmd)
        cmd = f'docker tag {current_tag} {new_tag}'
        print ("Command to retag image: {}".format(cmd))
        os.system(cmd)
        cmd = f'docker push {new_tag}'
        print ("Command to push image: {}".format(cmd))
        os.system(cmd)
        cmd = f'docker image rm {current_tag} {new_tag}'
        os.system(cmd)
        print ("Command to remove local image: {}".format(cmd))
        print("image promotion completed for {}".format(imageName))
ImagePromotion()